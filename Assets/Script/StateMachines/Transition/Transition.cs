﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class for transitions
/// </summary>
public class Transition
{
    public System.Func<bool> condition;
    public BaseState targetState;
    //Constructor to save the conditions and target states
    public Transition(System.Func<bool> condition, BaseState targetState)
    {
        this.condition = condition;
        this.targetState = targetState;
    }
}
