﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class from that the states can inherit
/// </summary>
public abstract class BaseState
{
    //List where the transition to other states are saved
    public List<Transition> transitions;
    //Basic State function
    public abstract void OnEnter();
    public abstract void OnExecute();
    public abstract void OnExit();
    //constructor for the State
    public BaseState()
    {
        transitions = new List<Transition>();
    }
    //Function to save transition and condition for the change
    public void AddTransition(System.Func<bool> condition, BaseState targetState)
    {
        transitions.Add(new Transition(condition, targetState));
    }
}