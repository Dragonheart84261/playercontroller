﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveState : BaseState
{
    PlayerController player;

    public PlayerMoveState(PlayerController player)
    {
        this.player = player;
    }

    public override void OnEnter()
    {

    }

    public override void OnExecute()
    {
        Vector2 moveDir = new Vector2(player.Horizontal, player.Vertical);
        Vector2 pos = player.gameObject.transform.position;
        player.transform.position = pos + moveDir * player.MovementSpeed * Time.deltaTime;
    }

    public override void OnExit()
    {

    }
}
