﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base StateMachine from that all StateMachines can inherit
/// </summary>
public abstract class BaseStateMachine
{
    //Protected variable so that every statemachine can use it
    protected BaseState currentState;

    //Getter for current state info
    public BaseState CurrentState { get => currentState;}

    public virtual void Execute()
    {
        currentState.OnExecute();
        //Only the Conditions that are saved in the current state getting called
        CheckTransitions();
    }

    public virtual void ChangeCurrentState(BaseState newState)
    {
        if (currentState != null)
        {
            currentState.OnExit();
        }

        currentState = newState;

        currentState.OnEnter();
    }
    
    //Checking the transition from current state
    public virtual void CheckTransitions()
    {
        foreach (var transition in currentState.transitions)
        {
            if (transition.condition())
            {
                ChangeCurrentState(transition.targetState);
                break;
            }
        }
    }

    //Function to define the condition for the transition
    public abstract void DefineTransition();
}