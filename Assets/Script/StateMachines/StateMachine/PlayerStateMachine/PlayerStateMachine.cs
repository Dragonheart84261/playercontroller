﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateMachine : BaseStateMachine
{
    PlayerController player;
    BaseState playerIdleState;
    BaseState playerMoveState;
    //Constructor for the Player statemachine
    public PlayerStateMachine(PlayerController player)
    {
        this.player = player;
        playerIdleState = new PlayerIdleState();
        playerMoveState = new PlayerMoveState(player);

        DefineTransition();
        //set the first state
        ChangeCurrentState(playerIdleState);
    }

    //You can define as many transitions as you want for a state
    public override void DefineTransition()
    {
        playerIdleState.AddTransition(IsMoving, playerMoveState);

        playerMoveState.AddTransition(DoNothing, playerIdleState);
    }

    // From here are the Conditions that are needed for the transitions
    bool IsMoving()
    {
        return player.Horizontal > 0 || player.Horizontal < 0 || player.Vertical > 0 || player.Vertical < 0; 
    }

    bool DoNothing()
    {
        return player.Horizontal == 0 && player.Horizontal == 0 && player.Vertical == 0 && player.Vertical == 0;
    }
}
