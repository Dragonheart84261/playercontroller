﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A stripped down version of a PlayerController
/// </summary>
public class PlayerController : MonoBehaviour
{
    [SerializeField] float movementSpeed;
    [SerializeField]string currentState;

    BaseStateMachine stateMachine;

    float horizontal, vertical;

    public float Horizontal { get => horizontal;}
    public float Vertical { get => vertical;}
    public float MovementSpeed { get => movementSpeed;}

    void Start()
    {
        stateMachine = new PlayerStateMachine(this);
    }

    void Update()
    {
        UpdateInput();
        stateMachine.Execute();
        UpdateStateInfo();
    }
    /// <summary>
    /// Update for the input
    /// </summary>
    void UpdateInput()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
    }
    /// <summary>
    /// Update for the State info for the inspector and to see if the change works as intended 
    /// </summary>
    void UpdateStateInfo()
    {
        currentState = stateMachine.CurrentState.ToString();
    }
}